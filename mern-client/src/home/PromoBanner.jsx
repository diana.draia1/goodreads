import React from 'react'
import { Link } from 'react-router-dom'
import bookPic from "../assets/awardbooks.png"

const PromoBanner = () => {
  return (
    <div className='mt-16 py-12 bg-teal-100 px-4 lg:px-24'>
        <div className='flex flex-col md:flex-row justify between items-center gap-12'>
            <div className='md:w-1/2'>
                <h2 className='text-4xl font-bold mb-6 leading-snug'>2023 National Book Awards for Fiction Shortlist</h2>
                <p className='mb-10 text-lg md:w-5/6'>No matter your reading preferences or interests, you'll find a world of stories waiting to captivate your imagination. So, dive in, explore our shelves, and embark on a new reading journey today. Your next favorite book is just a click away. Happy reading!</p>
                <Link to="/shop" className=' block'><button className='bg-blue-700 text-white font-semibold px-5 py-2 rounded hover:bg-black transition-all duration-300'>Explore More Books</button></Link>
            </div>

            <div>
                <img src={bookPic} alt='' className='w-96 rounded'/>
            </div>
        </div>
    </div>
  )
}

export default PromoBanner