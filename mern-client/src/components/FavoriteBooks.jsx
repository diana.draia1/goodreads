import React, { useContext, useEffect, useState } from 'react'
import { AuthContext } from '../contects/AuthProvider';


const FavoriteBooks = () => {
  const { favorites,addToFavorites, removeFromFavorites} = useContext(AuthContext);
  const [books, setBooks] = useState([]);


  useEffect( () => {
    fetch("http://localhost:5000/all-books").then(res => res.json()).then(data => setBooks(data));
  }, [])


  return (
    <div className='mt-28 px-4 lg:px24' >
       {/* Secțiunea de favorite */}
       <div className="my-4">
        <h2 className='text-3xl '>Favorites:</h2>
        <ul>
          {favorites.map(fav => (
            <li key={fav._id}>
              {fav.bookTitle}
              <button
                className="ml-2 mx-2 my-2 text-white bg-blue-200 hover:bg-blue-300 rounded cursor-pointer"
                onClick={() => removeFromFavorites(fav._id)}
              >
                remove
          </button>
            </li>
          ))}
        </ul>
      </div>

    </div>
  )
}

export default FavoriteBooks