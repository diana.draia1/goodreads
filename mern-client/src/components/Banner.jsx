import React from 'react'
import BannerCard from '../home/BannerCard'

const Banner = () => {
  return (
    <div className='px-4 lg:px-24 bg-teal-100 flex items-center'>
      <div className='flex w-full flex-col md:flex-row justify-between items-center gap-12 py-40'>
        {/* left side */}  
        <div className='md:w-1/2 space-y-8 h-full'>
          <h2 className='text-5xl font-bold leading-snug text-black'>Books <span className='text-green-700'>Rediscover Treasures, Share Stories</span></h2>
          <p className='md:w-4/5'> Join our community of book enthusiasts to find books, and let the pages of knowledge and imagination continue their journey in new hands.</p>
          <div>
            <input type="search" name="search" id="search" placeholder='Search a book' className='py-2 px-2 rounded-s-sm outline-none'/>
            <button className='bg-green-700 px-6 py-2 text-white font-medium hover:bg-green-500 transition-all ease-in duration-200'>Search</button>
          </div>
        </div>

        {/* right side */}
        <div>
          <BannerCard> </BannerCard>
        </div>
      </div>
    </div>
  )
}

export default Banner