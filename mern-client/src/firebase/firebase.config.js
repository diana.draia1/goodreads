// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAM-BEA6Mmt87RLIwXERKojTjAE3kdvins",
  authDomain: "goodreads-35238.firebaseapp.com",
  projectId: "goodreads-35238",
  storageBucket: "goodreads-35238.appspot.com",
  messagingSenderId: "963714994687",
  appId: "1:963714994687:web:4abed32cf0668923cbd7ca"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;