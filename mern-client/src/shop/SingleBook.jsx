import React, { useContext } from 'react';
import { useLoaderData } from 'react-router-dom';
import './SingleBook.css';
import { AuthContext } from '../contects/AuthProvider';

const SingleBook = () => {
  const {_id, bookTitle, imageURL, authorName, category, bookDescription, bookPDFURL } = useLoaderData();
  const { favorites, addToFavorites, removeFromFavorites } = useContext(AuthContext);
  const isBookInFavorites = favorites.some(fav => fav._id === _id);

  const handleToggleFavorite = () => {
    if (isBookInFavorites) {
      removeFromFavorites(_id);
    } else {
      addToFavorites({
        _id,
        bookTitle,
        authorName,
        imageURL,
        category,
        bookDescription,
        bookPDFURL
      });
    }
  };
  
  return (
    <div className='single-book-container'>
      <img src={imageURL} alt={bookTitle} className='single-book-image' />
      <h2 className='single-book-title'>{bookTitle}</h2>
      <h3 className='single-book-author'>{authorName}</h3>
      <p className='single-book-info'>Category: {category}</p>
      <p className='single-book-info'>Book description: {bookDescription}</p>
      <p className='single-book-info'><button className='add-to-favorite-button'> <a href={bookPDFURL} target="_blank" rel="noopener noreferrer">More book INFO</a></button> </p>
      
        {/* <button className='add-to-favorite-button'>Add to Favorite Now</button> */}
        <p className='single-book-info'>
        <button className='add-to-favorite-button' onClick={handleToggleFavorite}>
          {isBookInFavorites ? "Remove from Favorites" : "Add to Favorites"}
        </button>
      </p>
    </div>
  );
}

export default SingleBook;
