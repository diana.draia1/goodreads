import React, { useState, useEffect } from 'react';
import './Dashboard.css'


const Dashboard = () => {

  const [numBooks, setNumBooks] = useState(0);

  useEffect(() => {
    fetch("http://localhost:5000/all-books")
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
        // setAllBooks(data);
        setNumBooks(data.length);
      });
  }, []);

  return (
    <div className="dashboard-container">
  <div className="dashboard-content">
    <h1 className="dashboard-title">Numărul total de cărți introduse: {numBooks}</h1>
  </div>
</div>
  )
}

export default Dashboard