'use client';
import { Sidebar } from 'flowbite-react';
import { HiArrowSmRight, HiChartPie, HiInbox, HiOutlineCloudUpload, HiShoppingBag, HiTable, HiUser, HiViewBoards } from 'react-icons/hi';

import userImg from "../assets/profile.jpg"

export const SideBar = () => {
  return (

    <Sidebar aria-label="Sidebar with content separator example" className='p-1'>
        <Sidebar.Logo
        href="#"
        img={userImg}
        imgAlt="Flowbite logo"
      >
        <p>
          Admin
        </p>
      </Sidebar.Logo>
    <Sidebar.Items >
      <Sidebar.ItemGroup>
        <Sidebar.Item
          href="/admin/dashboard"
          icon={HiChartPie}
        >
          <p>
            Dashboard
          </p>
        </Sidebar.Item>
        <Sidebar.Item
          href="/admin/dashboard/upload"
          icon={HiOutlineCloudUpload}
        >
          <p>
            Upload Book
          </p>
        </Sidebar.Item>
        <Sidebar.Item
          href="/admin/dashboard/manage"
          icon={HiInbox}
        >
          <p>
            Manage Books
          </p>
        </Sidebar.Item>
        <Sidebar.Item
          href="/admin/dashboard/upload-category"
          icon={HiOutlineCloudUpload}
        >
          <p>
            Add Book Category
          </p>
        </Sidebar.Item>
        <Sidebar.Item
          href="/admin/dashboard/manage-category"
          icon={HiInbox}
        >
          <p>
            Modify Categories
          </p>
        </Sidebar.Item>
        <Sidebar.Item
          href="/"
          icon={HiShoppingBag}
        >
          <p>
            Home
          </p>
        </Sidebar.Item>
        
      </Sidebar.ItemGroup>
      
    </Sidebar.Items>
  </Sidebar>
  )
}