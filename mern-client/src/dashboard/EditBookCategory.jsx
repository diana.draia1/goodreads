import React, { useState } from 'react'
import { Button, Checkbox, Label, Select, TextInput, Textarea } from 'flowbite-react';
import { useLoaderData, useParams } from 'react-router-dom';

const EditBookCategory = () => {
    const {id} = useParams();
    const {category} = useLoaderData();

    

  //handle book submission
  const handleUpdate = (event) => {
      event.preventDefault();
      const form = event.target;

      const category = form.category.value;

      const updateBookObj = {
        category
      }

      //console.log(bookObj);
      //update book data
      fetch(`http://localhost:5000/category/${id}`, {
        method:"PATCH",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(updateBookObj)
      }).then(res => res.json()).then(data => {
        // console.log(data)
        alert("Category updated successfully!")
      })
  }


  return (
    <div className='px-4 my-12'>
      <h2 className='mb-8 text-3xl font-bold'>Update the Book Category data</h2>

      <form onSubmit={handleUpdate}  className="flex lg:w-[1180px] flex-col flex-wrap gap-4">
        {/* first row */}
      <div className='flex gap-8'>
          <div className='lg:w-1/2'>
            <div className="mb-2 block">
              <Label
                htmlFor="category"
                value="Book Category"
              />
            </div>
            <TextInput
              id="category"
              name='category'
              //placeholder="Book Category"
              required
              type="text"
              defaultValue={category}
            />
          </div>
      </div>

    
      <Button type="submit" className='mt-5'>
        Update Book
      </Button>
     
    </form>
    </div>
  )
}

export default EditBookCategory