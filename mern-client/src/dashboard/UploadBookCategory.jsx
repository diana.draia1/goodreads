import React, { useState } from 'react'
import { Button, Checkbox, Label, Select, TextInput, Textarea } from 'flowbite-react';

const UploadBookCategory = () => {


  //handle book submission
  const handleCategorySubmit = (event) => {
      event.preventDefault();
      const form = event.target;

      const category = form.category.value;

      const bookObj = {
        category
      }

      //console.log(bookObj);
      //send data to db
      fetch(`http://localhost:5000/upload-category`, {
        method:"POST",
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify(bookObj)
      }).then(res => res.json()).then(data => {
        // console.log(data)
        alert("Category uploaded successfully!")
        form.reset();
      })
  }


  return (
    <div className='px-4 my-12'>
      <h2 className='mb-8 text-3xl font-bold'>Upload New Book Category</h2>

      <form onSubmit={handleCategorySubmit}  className="flex lg:w-[1180px] flex-col flex-wrap gap-4">
        {/* first row */}
      <div className='flex gap-8'>
          <div className='lg:w-1/2'>
            <div className="mb-2 block">
              <Label
                htmlFor="category"
                value="Book New Category"
              />
            </div>
            <TextInput
              id="category"
              name='category'
              placeholder="Write New Book Category"
              required
              type="text"
            />
          </div>
      </div>

    
      <Button type="submit" className='mt-5'>
        Upload Book Category
      </Button>
     
    </form>
    </div>
  )
}

export default UploadBookCategory